var exec = require('cordova/exec');

module.exports = {
    get: function(success, error, options) {
        exec(success, error, "IMEI", "get", [options]);
    },
    getDeviceId: function(success, error, options) {
        exec(success, error, "IMEI", "getDeviceId", [options]);
    }
};

