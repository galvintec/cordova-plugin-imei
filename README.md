cordova-plugin-IMEI
======================
To install this plugin, follow the [Command-line Interface Guide](http://cordova.apache.org/docs/en/edge/guide_cli_index.md.html#The%20Command-line%20Interface).

If you are not using the Cordova Command-line Interface, follow [Using Plugman to Manage Plugins](http://cordova.apache.org/docs/en/edge/guide_plugin_ref_plugman.md.html).

Exposes 2 methods:

- __get__: returns IMEI as string
- __getDeviceId__: returns Device ID as string. This value is unique by each combination of device, package name and signature.

Each method takes two arguments, success and error functions.

Usage examples:

    window.plugins.imei.get(
      function(imei) {
        console.log("got imei: " + imei);
      },
      function() {
        console.log("error loading imei");
      }
    );

    window.plugins.imei.getDeviceId(
      function(deviceId) {
        console.log("got deviceId: " + deviceId);
      },
      function() {
        console.log("error loading deviceId");
      }
    );
