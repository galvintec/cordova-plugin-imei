package com.plugin.IMEI;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;

import android.provider.Settings.Secure;
import android.Manifest;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.content.pm.PackageManager;

public class IMEIPlugin extends CordovaPlugin {

    private static final String GET = "get";
    private static final String GET_DEVICE_ID = "getDeviceId";

    public static final int REQ_CODE = 0;
    public static final int PERMISSION_DENIED_ERROR = 20;
    public static final String READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;

    private CallbackContext callbackContext;
    private JSONArray executeArgs;
    private String action;

    protected void getPermission(int requestCode) {
        cordova.requestPermission(this, requestCode, READ_PHONE_STATE);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        this.executeArgs = args;

        this.action = action;

        switch (action) {
            case GET:
                if (cordova.hasPermission(READ_PHONE_STATE)) {
                    getIMEI();
                } else {
                    getPermission(REQ_CODE);
                }
                break;
            case GET_DEVICE_ID:
                getDeviceId();
                break;
            default:
                sendPluginResponse(PluginResult.Status.INVALID_ACTION, "");
        }

        return true;
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
        for (int r : grantResults) {
            if (r == PackageManager.PERMISSION_DENIED) {
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, PERMISSION_DENIED_ERROR));
                return;
            }
        }

        switch (requestCode) {
            case REQ_CODE:
                getIMEI();
                break;
        }
    }

    private void getIMEI() throws JSONException {
        TelephonyManager telephonyManager = (TelephonyManager) this.cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String result = telephonyManager.getDeviceId();
        PluginResult.Status status = PluginResult.Status.OK;

        sendPluginResponse(status, result);
    }

    private void getDeviceId() {
        PluginResult.Status status = PluginResult.Status.OK;
        String result = Secure.getString(cordova.getActivity().getApplicationContext().getContentResolver(), Secure.ANDROID_ID);

        sendPluginResponse(status, result);
    }

    private void sendPluginResponse(PluginResult.Status status, String response) {
        callbackContext.sendPluginResult(new PluginResult(status, response));
    }
}
